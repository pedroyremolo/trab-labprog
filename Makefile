CFLAGS=-std=c99 -Wall -Werror -pedantic

clean:
	rm -f geranum
	rm -f cripto

run-cripto:
	make cripto
	./cripto
	make clean

run-geranum:
	make src/geranum
	src/geranum 13 17
	make ./src/clean
