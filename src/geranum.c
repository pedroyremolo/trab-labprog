/**
 * @file geranum.c
 * @author Pedro Lopes
 * @date 28 Jul 2018
 * @brief Código escrito o programa de geração das chaves.
 *
 * Esse código abrange a geração das chaves públicas, criptográficas e descriptográficas
 * para criptografia assimétrica de um texto. As chaves geradas serão escritas em dois
 * arquivos .txt. Tais arquivos e chaves serão usados em cripto.c.
 *
 * */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_DE_DIVISORES 64
#define POSICAO_PRIMO_UM 1
#define POSICAO_PRIMO_DOIS 2

/**
 * @brief Valida parametro digitado e, caso correto, retorna o número
 *
 * Verifica se o parâmetro digitado é um número, caso seja, verifica se é primo. Se primo,
 * retorna o número.
 *
 * @param numeroString Número em formato de string.
 * @return Número Primo como long.
 * */
long getNumeroPrimo(const char *numeroString);

/**
 * @brief Gera a chave utilizada na Criptografia Assimétrica de uma frase
 *
 * Através da fatoração de um número, chaveIntermediaria, buscamos um coprimo desse número,
 * esse coprimo será a nossa chave de criptografia.
 *
 * @param chaveIntermediaria Chave gerada pela multiplicação de (primNum - 1) * (segNum - 1)
 * @return Chave para criptografar a frase.
 * @see fatoraNumero
 * @see getCandidato
 * */
long getChaveCriptografia(long chaveIntermediaria);

/**
 * @brief Fatora um número qualquer
 *
 * @param numero Número a ser fatorado
 * @param divisores Vetor a ser preenchido com os divisores
 * @return Número de divisores do número
 *
 * @see getChaveCriptografia
 * */
long fatoraNumero(long numero, long *divisores);

/**
 * @brief Procura um candidato para a chaveCriptografia
 *
 * Procura um candidato, coprimo de chaveIntermediaria, procurando um não divisor de chaveIntemediaria.
 *
 * @param pCandidato O endereço de onde será armazenado o candidato
 * @param divisores Vetor dos divisores de chaveIntermediaria
 * @param numeroDeDivisores Número de divisores de chaveIntermediaria
 * @return Coprimo de chaveIntermediaria
 *
 * @see getChaveCriptografia
 * */
void getCandidato(long *pCandidato, const long *divisores, long numeroDeDivisores);

/**
 * @brief Gera a chave de descriptografia da criptografia assimétrica de uma frase
 *
 * @param chaveIntermediaria Chave gerada pela multiplicação de (primNum - 1) * (segNum - 1)
 * @param chaveCriptografia Chave utilizada na criptografia da frase
 *
 * @return Chave usadada para descriptografar a frase.
 * */
const long getChaveDescriptografia(const long chaveIntermediaria, const long chaveCriptografia);

/**
 * @brief Verifica se um número é primo
 *
 * @param num Número a ser verificado
 *
 * @return 1 se primo, 0 caso contrário
 * */
int isPrimo(long num);

/**
 *@brief Função responsável por gerar .txt com as chaves
 *
 * @param chavePublica Chave pública, disponível para ambos as partes
 * @param chavePrivada Chave privada, de criptografia ou descriptografia, disponível apenas para uma das partes.
 * */
void geraTxT(long chavePublica, long chavePrivada, char *nomeDoArquivo);

int main(int argc, char const *argv[]) {
    if (argc != 3) {
        printf("Número de parâmetros inválido. Para bom funcionamento do programa, insira dois números primos como parâmetros\n");
        return 0;
    }

    long primNum = getNumeroPrimo(argv[POSICAO_PRIMO_UM]), segNum = getNumeroPrimo(argv[POSICAO_PRIMO_DOIS]);

    const long chavePublica = primNum * segNum;
    const long chaveIntermediaria = (primNum - 1) * (segNum - 1);
    const long chaveCriptografia = getChaveCriptografia(chaveIntermediaria);
    const long chaveDescriptografia = getChaveDescriptografia(chaveIntermediaria, chaveCriptografia);

    //Log Resultados
    printf("Primo 1: %ld \nPrimo 2: %ld \nchavePublica: %ld \nchaveIntermediaria: %ld \nchaveCriptografia: %ld \nchaveDescriptografia: %ld \n",
           primNum, segNum, chavePublica, chaveIntermediaria, chaveCriptografia, chaveDescriptografia);

    geraTxT(chavePublica, chaveCriptografia, "numcripto.txt");
    geraTxT(chavePublica, chaveDescriptografia, "numdescripto.txt");
    return 0;
}

long getNumeroPrimo(const char *numeroString) {
    long numero;

    for (int i = 0; i < strlen(numeroString); ++i) {
        if (!isdigit(numeroString[i])) {
            printf("O parâmetro digitado não é um número, por favor tente novamente.");
            exit(0);
        }
    }

    numero = atol(numeroString);
    if (!isPrimo(numero)) {
        printf("O número %ld não é primo, por favor, insira um primo e tente novamente", numero);
        exit(0);
    }

    return numero;
}

int isPrimo(long num) {
    long divisor = 2;
    while (num % divisor != 0) {
        divisor++;
    }
    if (num == divisor) return 1;
    return 0;
}

long getChaveCriptografia(long chaveIntermediaria) {
    long divisores[MAX_DE_DIVISORES];
    long numeroDeDivisores;
    long candidato;

    numeroDeDivisores = fatoraNumero(chaveIntermediaria, divisores);

    candidato = divisores[numeroDeDivisores - 1] + 1;
    getCandidato(&candidato, divisores, numeroDeDivisores);
    return candidato;
}

long fatoraNumero(long numero, long *divisores) {
    long divisor = 2;
    long numeroDeItens = 0;
    while (divisor <= numero) {
        while ((numero % divisor) == 0) {
            numero = numero / divisor;
            if (numeroDeItens == 0) {
                divisores[numeroDeItens] = divisor;
                numeroDeItens++;
            } else if (divisores[numeroDeItens - 1] != divisor) {
                divisores[numeroDeItens] = divisor;
                numeroDeItens++;
            }
        }
        divisor++;
    }

    return numeroDeItens;
}
//TODO Melhorar getCandidato para conseguir pegar números coprimos entre os divisores
void getCandidato(long *pCandidato, const long *divisores, long numeroDeDivisores) {
    int achouCandidato = 0;
    while (!achouCandidato) {
        int i = 0;
        int candidatoValido = 1;
        while (numeroDeDivisores - 1 != i) {
            if (*pCandidato % divisores[i] == 0) {
                candidatoValido = 0;
            }
            i++;
        }
        if (candidatoValido) achouCandidato = 1;
        else (*pCandidato)++;
    }
}

const long getChaveDescriptografia(const long chaveIntermediaria, const long chaveCriptografia) {
    long candidato = 1;

    while ((chaveCriptografia * candidato) % chaveIntermediaria != 1) {
        candidato++;
    }

    return candidato;
}

void geraTxT(long chavePublica, long chavePrivada, char *nomeDoArquivo) {
    FILE *arquivo;
    arquivo = fopen(nomeDoArquivo, "w");
    fprintf(arquivo, "%ld,%ld", chavePublica, chavePrivada);
    fclose(arquivo);
}
