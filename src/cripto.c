#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#define MAX_CASAS_DECIMAIS 50
#define NUM_MAX_CARACTERES 1000

/**
 * @brief Verifica se o arquivo escolhido é valido, no caso de entrada, e retorna o arquivo.
 *
 * @param argv Argumentos passados ao iniciar o programa.
 * @param leitura Parâmetro onde é informado se e arquivo será de leitura, true, ou não, false.
 *
 * @return Endereço do arquivo
 * */
FILE *getArquivo(char **argv, bool leitura);

/**
 * @brief Função responsável por ler a primeira opção encontrada como parâmetro do programa.
 *
 * @param argc Número de argumentos passados ao iniciar o programa.
 * @param argv Argumentos passados ao iniciar o programa.
 *
 * @return Caracter posterior ao - passado na inicialização
 * */
char getOpcao(int argc, char **argv);

/**
 * @brief Função que encapsula a lógica de negócio para criptografar o texto
 *
 * Função responsável por encapsular a lógica para criptografar um arquivo de texto.
 *
 * @param arquivoLeitura Endereço do arquivo de onde será lido o texto a ser criptografado.
 * @param arquivoEscrita Endereço do arquivo onde será escrita a informação criptografada.
 * @see calcExpMod
 * @see escreveCaractCriptInt
 * */
int criptografa(FILE *arquivoLeitura, FILE *arquivoEscrita);

/**
 * @brief Função que encapsula a lógica de negócio para descriptografar o texto
 *
 * Função responsável por encapsular a lógica para descriptografar o código presente no arquivo
 * criptografado anteriormente.
 *
 * @param arquivoLeitura Endereço do arquivo de onde será lido o código a ser descriptografado.
 * @param arquivoEscrita Endereço do arquivo a ser escrito o texto descriptografado.
 * @see getCaractCriptInt
 * */
int descriptografa(FILE *arquivoLeitura, FILE *arquivoEscrita);

/**
 * @brief Calcula a Exponenciação Modular
 *
 * Realiza o cálculo de (caract ^ exp) % mod. É utilizada tanto na criptografia quanto na
 * descriptografia.
 *
 * @param caract
 * @param exp
 * @param mod
 * @return O resultado do cálculo (caract ^ exp) % mod
 * */
int calcExpMod(int caract, int exp, int mod);

/**
 * @brief Converte um inteiro para o equivalente em binário na ordem invertida e no formato de string.
 *
 * Converte um inteiro, positivo, para o equivalente em binário na ordem invertida e no formato de string.
 *
 * @param num Número inteiro positivo a ser convertido.
 * @param numBin String onde será carregado o número(num) em binário.
 * */
void iToBInvertido(int num, char *numBin);

/**
 * @brief Função responsável por escrever o código criptográfico no arquivo
 *
 * Função responsável por escrever o código criptográfico no arquivo. Tal
 * código é composto pela concatenação do código ASCII do caracter criptografado
 * com um espaço.
 *
 * @param caractCript Código ASCII do caracter criptografado
 * @param arquivoEscrita Endereço do arquivo onde será escrita a informação criptografada.
 * @see iToA
 * */
void escreveArqCriptografado(int caractCript, FILE *arquivoEscrita);

/**
 * @brief Converte um inteiro positivo em string
 *
 * Converte um inteiro positivo em sua string decimal. Exemplo: num = 10 equivalerá
 * a *numString = "10".
 *
 * @param num Número inteiro positivo a ser convertido.
 * @param numString Número decimal em formato String
 * @see inverteString
 * */
void iToA(int num, char *numString);

/**
 * @brief Inverte uma dada string
 *
 * Ex: A string "casa" virará "asac".
 *
 * @param string String a ser invetida
 * */
void inverteString(char string[]);

/**
 * @brief Função responsável por decodificar, e escrever o texto descriptografado num arquivo.
 *
 * @param arquivoLeitura Endereço do Arquivo que contém o texto criptografado
 * @param arquivoEscrita Endereço do Arquivo onde será escrito o texto descriptografado
 * @param chavePublica A chave pública utilizada na criptografia e descriptografia
 * @param chaveDescripto Chave privada utilizada para descriptografar o arquivo.
 * */
int escreveArqDescriptografado(FILE *arquivoLeitura, FILE *arquivoEscrita, int chavePublica, int chaveDescripto);

int main(int argc, char *argv[]) {

    if (argc != 6) {
        printf("Para bom funcionamento do programa, por favor insira os 5 argumentos necessários\n");
        return 0;
    }

    FILE *arquivoLeitura = getArquivo(argv, true);
    FILE *arquivoEscrita = getArquivo(argv, false);

    //FIXME Melhorar validação das entradas para escolher qual código irá rodar (cripto -c ou descripto -d)
    char opcao = getOpcao(argc, argv);

    if (opcao == 'c') criptografa(arquivoLeitura, arquivoEscrita);
    else if (opcao == 'd') descriptografa(arquivoLeitura, arquivoEscrita);
    else {
        printf("Opção -%c inválida, tente novamente selecionando -c (para criptografar) ou -d (para descriptografar)",
               opcao);
        return 0;
    }

    fclose(arquivoLeitura), fclose(arquivoEscrita);

    return 0;
}

FILE *getArquivo(char **argv, bool leitura) {
    char *comandoBuscado;
    FILE *arquivo;
    char *caminhoDoArquivo;
    int posicaoParametro = 1;

    comandoBuscado = leitura ? "<" : ">";

    while (strcmp(*argv, comandoBuscado) != 0) {
        argv++;
        posicaoParametro++;
    }

    argv++;
    caminhoDoArquivo = *argv;

    arquivo = fopen(caminhoDoArquivo, leitura ? "r" : "w");
    if (leitura && arquivo == NULL) {
        printf("Arquivo a ser lido não foi encontrado, por favor tente novamente");
        exit(0);
    }

    return arquivo;
}

char getOpcao(int argc, char **argv) {

    for (int i = 0; i < argc; ++i) {
        if (*argv[0] == '-' && strlen(*argv) == 2) {
            (*argv)++;
            return **argv;
        }
        argv++;
    }

    return 0;
}

int criptografa(FILE *arquivoLeitura, FILE *arquivoEscrita) {

    FILE *numCriptoTxt;
    int chavePublica, chaveCripto;

    numCriptoTxt = fopen("numcripto.txt", "r");

    fscanf(numCriptoTxt, "%d,%d", &chavePublica, &chaveCripto);
    printf("chavePublica: %d\nchaveCripto: %d\n", chavePublica, chaveCripto);

    int caract;

    while ((caract = fgetc(arquivoLeitura)) != EOF) {
        printf("%d ", caract);
        int caractCript = calcExpMod(caract, chaveCripto, chavePublica);

        printf("%d ", caractCript);

        escreveArqCriptografado(caractCript, arquivoEscrita);
    }
    return 0;
}

void escreveArqCriptografado(int caractCript, FILE *arquivoEscrita) {
    char caractCriptInt[MAX_CASAS_DECIMAIS];
    iToA(caractCript, caractCriptInt);
    strcat(caractCriptInt, " ");

    fputs(caractCriptInt, arquivoEscrita);
}

void iToA(int num, char *numString) {
    int i;
    i = 0;
    do {
        numString[i++] = num % 10 + '0';
    } while ((num /= 10) > 0);
    numString[i] = '\0';
    inverteString(numString);
}

void inverteString(char string[]) {
    int aux, i, j;
    for (i = 0, j = strlen(string) - 1; i < j; i++, j--) {
        aux = string[i];
        string[i] = string[j];
        string[j] = aux;
    }
}

int descriptografa(FILE *arquivoLeitura, FILE *arquivoEscrita) {
    FILE *numDescriptoTxt;
    int chavePublica, chaveDescripto;

    numDescriptoTxt = fopen("numdescripto.txt", "r");

    fscanf(numDescriptoTxt, "%d,%d", &chavePublica, &chaveDescripto);
    printf("chavePublica: %d\nchaveDescripto: %d\n", chavePublica, chaveDescripto);

    escreveArqDescriptografado(arquivoLeitura, arquivoEscrita, chavePublica, chaveDescripto);

    return 0;
}

int escreveArqDescriptografado(FILE *arquivoLeitura, FILE *arquivoEscrita, int chavePublica, int chaveDescripto) {
    char caractCriptInt[NUM_MAX_CARACTERES][MAX_CASAS_DECIMAIS];
    int numLetra = 0;

    while (1) {
        int digito = fgetc(arquivoLeitura);
        int i = 0;
        while (digito != ' ' && !feof(arquivoLeitura)) {
            caractCriptInt[numLetra][i++] = digito;
            digito = fgetc(arquivoLeitura);
        }
        caractCriptInt[numLetra][i] = 0;
        if (feof(arquivoLeitura)) {
            break;
        }
        numLetra++;
    }

    for (int j = 0; j < numLetra; ++j) {
        int caracter = atoi(caractCriptInt[j]);
        caracter = calcExpMod(caracter, chaveDescripto, chavePublica);
        fputc(caracter, arquivoEscrita);
    }

    return 0;
}

int calcExpMod(int caract, int exp, int mod) {
    char numBin[65];
    int numDeBits;
    long result;
    iToBInvertido(exp, numBin);

    numDeBits = strlen(numBin);

    int resultExpModular[numDeBits];

    result = 1;
    for (int potDe2 = 0; potDe2 < numDeBits; ++potDe2) {
        if (potDe2 == 0) resultExpModular[0] = caract % mod;
        else resultExpModular[potDe2] = (resultExpModular[potDe2 - 1] * resultExpModular[potDe2 - 1]) % mod;
        if (numBin[potDe2] == '1') result *= resultExpModular[potDe2];
    }

    result = result % mod;

    return (int) result;
}

void iToBInvertido(int num, char *numBin) {
    int i;
    i = 0;
    do {
        numBin[i++] = (num % 2) + '0';
    } while ((num /= 2) > 0);

    numBin[i] = '\0';
}
